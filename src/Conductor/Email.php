<?php
namespace Conductor;

class Email  {

    public function __construct() {  
        
    }

    public static function send( $title, $message, $to=null, $from=null, $key=null, $replyTo=null )
    {
        error_reporting(E_ALL);
        ini_set('display_errors', 1);

        if( !isset( $to ) or !is_array( $to ) )
            $to['email'] = 'broker@rnked.com';

        if( !isset( $from ) or !is_array( $from ) )
        {
            $from['email'] = 'no-reply@rnked.com';
            $from['name'] = 'Rnked';
        }

        $url = 'https://api.sendgrid.com/';
        $pass = $key;

        $params = array(
            'to'        => $to['email'],
            'from'      => $from['email'],
            'fromname'  => $from['name'],
            'subject'   => $title,
            'html'      => Email::emailWrapper( $title, $message )
          );

        if( isset( $replyTo ) )
        $params['replyto'] = $replyTo;      
      
        $request =  $url.'api/mail.send.json';

        // Generate curl request
        $session = curl_init($request);
        // Tell PHP not to use SSLv3 (instead opting for TLS)
        //curl_setopt($session, CURLOPT_SSLVERSION, CURL_SSLVERSION_TLSv1_2);
        curl_setopt($session, CURLOPT_HTTPHEADER, array('Authorization: Bearer ' .$key ));
        // Tell curl to use HTTP POST
        curl_setopt ($session, CURLOPT_POST, true);
        // Tell curl that this is the body of the POST
        curl_setopt ($session, CURLOPT_POSTFIELDS, http_build_query( $params ) );
        // Tell curl not to return headers, but do return the response
        curl_setopt($session, CURLOPT_HEADER, false);
        curl_setopt($session, CURLOPT_RETURNTRANSFER, true);

        // obtain response
        $response = curl_exec($session);

        curl_close($session);

    }

    public static function emailWrapper( $title, $message )
    {

        $message_html = ''.nl2br( $message ).'';

        return $message_html;
    }
    
    public static function array2Table( $array )
    {
        $excludes = array( 'action', 'is_ajax', 'id' );
        $array = array_diff_key( $array, array_flip( $excludes ) );
        
        $message = '<table>';
        foreach( $array as $key => $value )
        {
            if( is_array( $value ) ) $value = implode( ', ', $value );
            if( is_numeric( $value) && strpos( $key, 'phone'  ) === false ) $value = number_format( $value, 0 );
          
            if( strpos( $key, 'id'  ) == 0 )
              $message .= '<tr><td><strong>'.ucwords( str_replace( '_', ' ', $key ) ).'</strong></td><td>'.$value.'</td></tr>';
        }
        $message .= '</table>';

        return $message;

    }   
    
} 