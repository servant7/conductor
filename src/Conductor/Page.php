<?php
namespace Conductor;

class Page  {

    public function __construct() {
        
      
        //conductor config
        $this->conductorConfig = Utility::getConfig();      
      
        //domain paths
        $this->domainInfo = $this->getDomainInfo();

      
        //parse website settings from ini       
        if( isset( $this->domainInfo->path ) && file_exists( $this->domainInfo->path.'/config.json' ) )
        {            
            $config_json = file_get_contents ( $this->domainInfo->path.'/config.json' );
            $website_settings = json_decode( $config_json, true );                               
            $this->website = (object) $website_settings;
          
            if( !isset( $this->website->router ) )
                $this->website->router = 'default';
        }
        else{
            include( dirname(__FILE__).'/../templates/error-website.php' );
            die();
        }
      
              
        if( isset( $this->website->libraries ) && is_array( $this->website->libraries ) ){
            require_once( 'autoloader.php' );
            foreach( $this->website->libraries as $library ){
              autoloader( array( getcwd().'/'.$library ), false);
            }                            
        }      
      
        //cache settings
        //$this->pageCacheFile = base64_encode( reset((explode('?',  $_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'] ))) );      
        $this->pageCacheFile = str_replace(array('+', '/'), array('-', '_'), base64_encode( $_SERVER['HTTP_HOST'].ltrim($_SERVER['REQUEST_URI'], '/') ) );
        
        $this->pageCachePath = getcwd().'/cache/'.$this->pageCacheFile;         
        $this->fromCache = false;
        
    }    
    
    public static function getDomainInfo()
    {    
        //domain paths
        $info = array();
        
        $info['domain'] = $_SERVER['HTTP_HOST'];
        if( Utility::getConfig()->srv_dev )
            $info['domain'] = str_replace( '.'.Utility::getConfig()->srv_url, '', $_SERVER['HTTP_HOST'] );      
      
        //strip QS if exists
        if( strpos( $info['domain'], '?') )
            $info['domain'] = substr( $info['domain'], 0, strpos( $info['domain'], '?'));      
      
        if( isset( Utility::getConfig()->site_wildcard ) )
            $info['path'] = Utility::getConfig()->site_wildcard;       
      
        $info['baseUrl'] = ( isset( Utility::getConfig()->srv_dev ) )? '/'.$info['domain'].'/' : '/' ;
 

        if( file_exists( 'sites/'.$info['domain'] ) ){
            $info['path'] = getcwd().'/sites/'.$info['domain'];
        }
      
        return (object) $info;
        
    }
    
    private function clean( $html )
    {
        $config = array(
           'indent'               => true,
           'output-xhtml'         => false,
           'wrap'                 => 200,
           'hide-comments'        => true,
           'drop-empty-elements'  => false,
           'new-empty-tags'       =>'vue'
          
        );            
      
        // Clean HTML
        /**/
        $tidy = new \tidy;
        $tidy->parseString( $html, $config, 'utf8');
        $tidy->cleanRepair();
        $this->html =  $tidy;
      
        // Modify vue tags (for use of Vue CLI)
        $dom = new \DOMDocument;
        libxml_use_internal_errors(true);
        $dom->loadHTML( $this->html );      
      
        foreach( $dom->getElementsByTagName('vue') as $component ) {
       
            $tag = $component->getAttribute('v-name');
            $component->removeAttribute('v-name');
          
            $newnode = $component->ownerDocument->createElement( $this->domainInfo->domain.'-'.$tag, $component->nodeValue);
            
            foreach ($component->childNodes as $child){
                $child = $component->ownerDocument->importNode($child, true);
                $newnode->appendChild($child);
            }
            foreach ($component->attributes as $attrName => $attrNode) {
                $attrName = $attrNode->nodeName;
                $attrValue = $attrNode->nodeValue;
                $newnode->setAttribute($attrName, $attrValue);;
            }
          
            $component->parentNode->replaceChild($newnode, $component);
            
          
        }     
        $this->html = $dom->saveHTML();            
      
        return $this->html;
      
    }       
      
    public function headers()
    {
        // Wrap html with proper headers
        header( 'Content-Type: text/html' );
        header( 'Content-language: en' );
        header( 'Last-Modified: '.gmdate('D, d M Y H:i:s', ( time() ) ).' GMT' );
        header( 'Content-Length: '.strlen( $this->html ) );
        header( 'Cache-Control: max-age=3600, must-revalidate' ); // must-revalidate
        header( 'Expires: '.gmdate('D, d M Y H:i:s', ( time() + 3600 ) ).' GMT' );
        header( 'Pragma: no-cache' );
        header( 'ETag: '.md5( $this->html ) );

        // New headers for security
        header( 'X-Frame-Options: DENY' );
        header( 'strict-transport-security: max-age=600' );
        header( 'X-Content-Type-Options: nosniff' );
        header( 'X-XSS-Protection: 1;mode=block' );
        header( 'X-Permitted-Cross-Domain-Policies: master-only' );
        header( 'Content-Security-Policy: ;' );
        header( 'X-Content-Security-Policy: ;' );

        header( 'Access-Control-Allow-Origin: *' );
        header( 'Access-Control-Allow-Headers: Origin, X-Requested-With, Content-Type, Accept' );   
    }  
    
    public function cacheSave()
    {
        if( !defined( 'NO_CACHE' ) )
        {
          
            $cacheFolder = getcwd().'/cache';
          
            //ensure that cache folder exists
            if( file_exists( $cacheFolder ) && is_writable( $cacheFolder ) ){                

                // Clear old cache files
                $files = glob( $cacheFolder.'/*' );
                foreach( $files as $file )
                {              
                    if ( filemtime( $file ) < time()-( $this->website->cache_timeout ) )
                    unlink($file);
                }
              
                // Write html to cache folder                
                $file = fopen( $cacheFolder.'/'.$this->pageCacheFile, 'wb' );
                fwrite( $file, $this->html );
                fclose( $file );
              
            }

        }
        
        return true;
    
    }     
    
    
    // Custom router
    private function process()
    {
        if( ( isset( Utility::getConfig()->srv_dev ) && Utility::getConfig()->srv_dev === false or !isset( Utility::getConfig()->srv_dev ) )   && file_exists( $this->pageCachePath ) && filesize( $this->pageCachePath ) > 0 )
        {        
            $html = file_get_contents( $this->pageCachePath );
            $this->fromCache = true;
            echo $html;
            return true;
        }             
        
        if( isset( $this->website->router ) && $this->website->router != '' && $this->website->router != 'default' )
        {
            include_once( $this->domainInfo->path.'/'.$this->website->router );    
        }
        else
        {

            //strip QS for page lookup
            if( strpos( $_GET['_qstring'], '?') )
                $_GET['_qstring'] = substr( $_GET['_qstring'], 0, strpos( $_GET['_qstring'], '?'));             
          
            //string traditional QS as needed (TEMP?)
            $url_array = array_values( array_filter( explode( '/', $_GET['_qstring'] ) ) );
            $url_count = count( $url_array );   
          
            //load home page
            if( !count( $url_array ) ) {    
                $this->pageFilename = 'home';
                $this->pagePath = $this->domainInfo->path.'/home.php'; 
                if( !file_exists( $this->pagePath )){
                    $this->pagePath = dirname(__FILE__).'/../templates/home.php';
                }               
              
            }            
            
            //load page with subdir
            if( !isset( $this->pagePath ) &&  $url_count > 1 ){
               $this->pageFilename = str_replace( '/', '_', trim( $_GET['_qstring'], '/' ) );
               $this->pagePath = $this->domainInfo->path.'/'.$this->pageFilename.'.php';
               if( !file_exists( $this->pagePath )){
                 unset(  $this->pageFilename );
                 unset(  $this->pagePath );
               }                   
            }
          
            //load single root page
            if( !isset( $this->pagePath ) ){
              $this->pageFilename = $url_array[0]; 
              $this->pagePath = $this->domainInfo->path.'/'.$this->pageFilename.'.php';
              if( !file_exists( $this->pagePath )){
                unset(  $this->pageFilename );
                unset(  $this->pagePath );
              }                
              
            }
          
            //if no page, load error
            if( !isset( $this->pagePath ) ){                
                if( file_exists( $this->domainInfo->path.'/error.php' ) )
                    $this->pagePath = $this->domainInfo->path.'/error.php';
                else
                    $this->pagePath =  dirname(__FILE__).'/../templates/error.php';
            }              
                        
            include_once( $this->pagePath );              
              
        }
        
          
          
        return true;

    }    
   
    
    // Custom router
    public function render()
    {
        
        ob_start();
        $this->process();
        $this->html = ob_get_clean();
        
        if( $this->fromCache == false )
        {          
            $this->html = $this->clean( $this->html );
            $this->cacheSave();
        }            
        
        return $this->html;        
    }        
 
    

} 