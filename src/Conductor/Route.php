<?php
namespace Conductor;

class Route {

    public function __construct() {
        $this->requestURI = $_SERVER['REQUEST_URI'];
        $this->indexFile = 'index.php';
        $this->scriptName = $_SERVER['SCRIPT_NAME'];
        $this->basePath = str_replace( $this->indexFile, '', $this->scriptName );
        $this->uri = ( !empty( $basePath ) && $this->basePath != '/' ) ? str_replace( $this->basePath, '', $this->requestURI ) : $this->requestURI ;
        
        
    }    
    
    
    private function mysql_injection_protection()
    {
        $mysql_function_list = array('SELECT','HEX', 'UNHEX','UNION','ABORT','ALTER DATABASE','ALTER GROUP','ALTER TABLE','ALTER TRIGGER','ALTER USER','ANALYZE','BEGIN','CHECKPOINT','CLUSTER','COMMIT','CREATE AGGREGATE','CREATE CAST','CREATE CONSTRAINT TRIGGER','CREATE CONVERSION','CREATE DATABASE','CREATE DOMAIN','CREATE FUNCTION','CREATE GROUP','CREATE INDEX','CREATE LANGUAGE','CREATE OPERATOR','CREATE OPERATOR CLASS','CREATE RULE','CREATE SCHEMA','CREATE SEQUENCE','CREATE TABLE','CREATE TABLE AS','CREATE TRIGGER','CREATE TYPE','CREATE USER','CREATE VIEW','DEALLOCATE','DECLARE','DELETE','DROP AGGREGATE','DROP CAST','DROP CONVERSION','DROP DATABASE','DROP DOMAIN','DROP FUNCTION','DROP GROUP','DROP INDEX','DROP LANGUAGE','DROP OPERATOR','DROP OPERATOR CLASS','DROP RULE','DROP SCHEMA','DROP SEQUENCE','DROP TABLE','DROP TRIGGER','DROP TYPE','DROP USER','DROP VIEW','EXECUTE','EXPLAIN','FETCH','GRANT','INSERT','LISTEN','LOAD','LOCK','MOVE','NOTIFY','PREPARE','REINDEX','RESET','REVOKE','ROLLBACK','SELECT','SELECT INTO','SET CONSTRAINTS','SET SESSION AUTHORIZATION','SET TRANSACTION','START TRANSACTION','TRUNCATE','UNLISTEN','UPDATE','VACUUM');
        
        if( !empty( $_GET ) && count( $_GET ) > 0 )
        {
            foreach( $_GET as $i => $value )
            {
                if( !is_array( $value ) )
                {
                    foreach( $mysql_function_list as $i => $function ){
                        if( preg_match( '/'.$function.'/i', $value  ) && !preg_match( '/_'.$function.'/i', $value ) && !preg_match( '/'.$function.'_/i', $value ) )
                        {                            
                            header('Location: //'.$_SERVER['SERVER_NAME']);
                            die('');
                        }
                    }
                }
            }
        }
    }
    
    /**
    * Create routes by adjusting GET values, and return a Router object
    *
    * @return Route
    */    
    public function process( $debug=false )
    {
        //check for raw file and return
        Utility::checkFile();
      
        //check for universal files
        Utility::checkFileUniversal();      
      
        $route = array();

        //check if arbitrary variabes exist
        // remove and save
        if( strpos( $this->requestURI, "?" ) !== false ){

            // $rq = $this->requestURI;
            list( $this->requestURI, $qs ) = explode("?", $this->requestURI);
            $_GET['_query_string'] = $qs;

        }

        $segments = explode( '/', $this->uri );
        $route['_qstring'] = '';

        for( $i = 1; $i <= count( $segments ) - 1; $i += 2 )
        {

            $var = $segments[$i];
            if( !empty( $var ) ){
                $value = ( isset( $segments[( $i+1 )] ) ) ? $segments[( $i+1 )] : '' ;
                $route[$var] = $value;
            }

        }

        foreach( $route as $key => $value ){
            if( !in_array( $key, array( '_qstring') ) )
                $route['_qstring'] .= '/'.$key.'/'.$value;

        }

        $_GET = array_merge( $_GET, $route );
        
        // Check for injection attempt
        //$this->mysql_injection_protection();        
        
        if( $debug ) print_r( $_GET );            
        
        return $_GET;
        
    }    

} 