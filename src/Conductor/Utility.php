<?php
namespace Conductor;

class Utility  {

    public function __construct() {
    }    
    
    public static function getConfig()
    {

        $env_settings = array();
        if( file_exists( getcwd() .'/env.json' ) ){

            $env_json = file_get_contents (getcwd() .'/env.json');
            $env_settings = json_decode( $env_json, true );
        }
        else
          $env_settings['srv_dev'] = false;
        
        $env_settings['conductor_path'] = getcwd() ;
      
        return (object) $env_settings;
        
    }  
  
    public static function checkFile()
    {

        if( $_SERVER['REQUEST_URI'] !=  '/' ){
          
            $domainInfo = Page::getDomainInfo();
          
            if( isset( $domainInfo->path ) ){
                $file_check = $domainInfo->path.(( isset( Utility::getConfig()->srv_dev ) && Utility::getConfig()->srv_dev )?  preg_replace( '/^\/'.$domainInfo->domain.'/', '', $_SERVER['REQUEST_URI']) : $_SERVER['REQUEST_URI']);

                if( file_exists( $file_check ) && is_file( $file_check ) ){
                      Utility::streamFile( $file_check );
                      die();      
                }              
            }

          
        }
        
        return false;
    }  
  
    public static function checkFileUniversal()
    {

        if( $_SERVER['REQUEST_URI'] !=  '/' ){
          
          $_SERVER['REQUEST_URI'] = preg_replace('/^\/universal/i', '', $_SERVER['REQUEST_URI'] );         
          $file_check = getcwd().'/sites/_universal'.$_SERVER['REQUEST_URI'];
          if( file_exists( $file_check ) && is_file( $file_check ) ){
                Utility::streamFile( $file_check );
                die();      
          }  
          
        }
        
        return false;
    }    
  
    public static function streamFile( $path )
    {
        //set headers
        $extension = pathinfo( $path, PATHINFO_EXTENSION );
        $mime = Utility::ext2mime( $extension );      
        $file_contents = file_get_contents( $path );
      
        header( 'Pragma: public'  );
        header( 'Cache-Control: max-age=86400'  );
        header( 'Expires: '. gmdate('D, d M Y H:i:s \G\M\T', time() + 86400)  );
        header( 'Content-Type: '.$mime );             
        header( 'Last-Modified: '.gmdate('D, d M Y H:i:s', ( time() ) ).' GMT' );
        header( 'ETag: '.md5( $file_contents ) ); 
        header( 'Content-Length: '.strlen ( $file_contents ) );
      
        //make files open for CORS
        header( 'Access-Control-Allow-Origin: *' );
      
        echo $file_contents;
        die();
      
        return true;        
    }
  
    public static function issetor( &$var, $default = false) 
    {
        $value = (isset($var)) ? $var : $default;
        if( $var == '' && $default == false ) $value = false;
        
        return $value;
    }
  
  
    public static function templatePath() 
    {
        return dirname(__FILE__).'/../templates/';
    }  
   
    public static function trunc($phrase, $max_words) {
        $phrase_array = explode(' ',$phrase);
      
        if(count($phrase_array) > $max_words && $max_words > 0)
        $phrase = implode(' ',array_slice($phrase_array, 0, $max_words)).'...';
      
        return $phrase;
    }  
  
    public static function redirect( $url )
    {

        header('location: '.$url );
        die();
    }


    public static function pre( $data )
    {

        echo '<pre>';
        print_r( $data );
        echo '</pre>';

    } 
  
    public static function ext2mime( $ext )
    {

        $types = array(
            'ai'      => 'application/postscript',
            'aif'     => 'audio/x-aiff',
            'aifc'    => 'audio/x-aiff',
            'aiff'    => 'audio/x-aiff',
            'asc'     => 'text/plain',
            'atom'    => 'application/atom+xml',
            'atom'    => 'application/atom+xml',
            'au'      => 'audio/basic',
            'avi'     => 'video/x-msvideo',
            'bcpio'   => 'application/x-bcpio',
            'bin'     => 'application/octet-stream',
            'bmp'     => 'image/bmp',
            'cdf'     => 'application/x-netcdf',
            'cgm'     => 'image/cgm',
            'class'   => 'application/octet-stream',
            'cpio'    => 'application/x-cpio',
            'cpt'     => 'application/mac-compactpro',
            'csh'     => 'application/x-csh',
            'css'     => 'text/css',
            'csv'     => 'text/csv',
            'dcr'     => 'application/x-director',
            'dir'     => 'application/x-director',
            'djv'     => 'image/vnd.djvu',
            'djvu'    => 'image/vnd.djvu',
            'dll'     => 'application/octet-stream',
            'dmg'     => 'application/octet-stream',
            'dms'     => 'application/octet-stream',
            'doc'     => 'application/msword',
            'dtd'     => 'application/xml-dtd',
            'dvi'     => 'application/x-dvi',
            'dxr'     => 'application/x-director',
            'eps'     => 'application/postscript',
            'etx'     => 'text/x-setext',
            'exe'     => 'application/octet-stream',
            'ez'      => 'application/andrew-inset',
            'gif'     => 'image/gif',
            'gram'    => 'application/srgs',
            'grxml'   => 'application/srgs+xml',
            'gtar'    => 'application/x-gtar',
            'hdf'     => 'application/x-hdf',
            'hqx'     => 'application/mac-binhex40',
            'htm'     => 'text/html',
            'html'    => 'text/html',
            'ice'     => 'x-conference/x-cooltalk',
            'ico'     => 'image/x-icon',
            'ics'     => 'text/calendar',
            'ief'     => 'image/ief',
            'ifb'     => 'text/calendar',
            'iges'    => 'model/iges',
            'igs'     => 'model/iges',
            'jpe'     => 'image/jpeg',
            'jpeg'    => 'image/jpeg',
            'jpg'     => 'image/jpeg',
            'js'      => 'application/x-javascript',
            'json'    => 'application/json',
            'kar'     => 'audio/midi',
            'latex'   => 'application/x-latex',
            'lha'     => 'application/octet-stream',
            'lzh'     => 'application/octet-stream',
            'm3u'     => 'audio/x-mpegurl',
            'man'     => 'application/x-troff-man',
            'mathml'  => 'application/mathml+xml',
            'me'      => 'application/x-troff-me',
            'mesh'    => 'model/mesh',
            'mid'     => 'audio/midi',
            'midi'    => 'audio/midi',
            'mif'     => 'application/vnd.mif',
            'mov'     => 'video/quicktime',
            'movie'   => 'video/x-sgi-movie',
            'mp2'     => 'audio/mpeg',
            'mp3'     => 'audio/mpeg',
            'mpe'     => 'video/mpeg',
            'mpeg'    => 'video/mpeg',
            'mpg'     => 'video/mpeg',
            'mpga'    => 'audio/mpeg',
            'ms'      => 'application/x-troff-ms',
            'msh'     => 'model/mesh',
            'mxu'     => 'video/vnd.mpegurl',
            'nc'      => 'application/x-netcdf',
            'oda'     => 'application/oda',
            'ogg'     => 'application/ogg',
            'pbm'     => 'image/x-portable-bitmap',
            'pdb'     => 'chemical/x-pdb',
            'pdf'     => 'application/pdf',
            'pgm'     => 'image/x-portable-graymap',
            'pgn'     => 'application/x-chess-pgn',
            'png'     => 'image/png',
            'pnm'     => 'image/x-portable-anymap',
            'ppm'     => 'image/x-portable-pixmap',
            'ppt'     => 'application/vnd.ms-powerpoint',
            'ps'      => 'application/postscript',
            'qt'      => 'video/quicktime',
            'ra'      => 'audio/x-pn-realaudio',
            'ram'     => 'audio/x-pn-realaudio',
            'ras'     => 'image/x-cmu-raster',
            'rdf'     => 'application/rdf+xml',
            'rgb'     => 'image/x-rgb',
            'rm'      => 'application/vnd.rn-realmedia',
            'roff'    => 'application/x-troff',
            'rss'     => 'application/rss+xml',
            'rtf'     => 'text/rtf',
            'rtx'     => 'text/richtext',
            'sgm'     => 'text/sgml',
            'sgml'    => 'text/sgml',
            'sh'      => 'application/x-sh',
            'shar'    => 'application/x-shar',
            'silo'    => 'model/mesh',
            'sit'     => 'application/x-stuffit',
            'skd'     => 'application/x-koan',
            'skm'     => 'application/x-koan',
            'skp'     => 'application/x-koan',
            'skt'     => 'application/x-koan',
            'smi'     => 'application/smil',
            'smil'    => 'application/smil',
            'snd'     => 'audio/basic',
            'so'      => 'application/octet-stream',
            'spl'     => 'application/x-futuresplash',
            'src'     => 'application/x-wais-source',
            'sv4cpio' => 'application/x-sv4cpio',
            'sv4crc'  => 'application/x-sv4crc',
            'svg'     => 'image/svg+xml',
            'svgz'    => 'image/svg+xml',
            'swf'     => 'application/x-shockwave-flash',
            't'       => 'application/x-troff',
            'tar'     => 'application/x-tar',
            'tcl'     => 'application/x-tcl',
            'tex'     => 'application/x-tex',
            'texi'    => 'application/x-texinfo',
            'texinfo' => 'application/x-texinfo',
            'tif'     => 'image/tiff',
            'tiff'    => 'image/tiff',
            'tr'      => 'application/x-troff',
            'tsv'     => 'text/tab-separated-values',
            'txt'     => 'text/plain',
            'ustar'   => 'application/x-ustar',
            'vcd'     => 'application/x-cdlink',
            'vrml'    => 'model/vrml',
            'vxml'    => 'application/voicexml+xml',
            'wav'     => 'audio/x-wav',
            'wbmp'    => 'image/vnd.wap.wbmp',
            'wbxml'   => 'application/vnd.wap.wbxml',
            'wml'     => 'text/vnd.wap.wml',
            'wmlc'    => 'application/vnd.wap.wmlc',
            'wmls'    => 'text/vnd.wap.wmlscript',
            'wmlsc'   => 'application/vnd.wap.wmlscriptc',
            'wrl'     => 'model/vrml',
            'xbm'     => 'image/x-xbitmap',
            'xht'     => 'application/xhtml+xml',
            'xhtml'   => 'application/xhtml+xml',
            'xls'     => 'application/vnd.ms-excel',
            'xml'     => 'application/xml',
            'xpm'     => 'image/x-xpixmap',
            'xsl'     => 'application/xml',
            'xslt'    => 'application/xslt+xml',
            'xul'     => 'application/vnd.mozilla.xul+xml',
            'xwd'     => 'image/x-xwindowdump',
            'xyz'     => 'chemical/x-xyz',
            'zip'     => 'application/zip'
          );        

        if( isset( $types[$ext] ) )
            return $types[$ext];
        else
            return 'text/plain';
    }  
  

    

} 