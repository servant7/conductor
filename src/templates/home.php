<?php
define( 'NO_CACHE', true );

/*Page Config*/
$this->pageTitle = $this->website->name;
$this->pageSEOTitle = $this->pageTitle;
$this->pageDescription = '';
$this->navPosition = 'fixed-top';
?>
<!DOCTYPE html>
<html lang="en-us">

    <head>
        <meta charset="utf-8" />
        
        <!-- mobile settings -->
        <meta name="viewport" content="width=device-width, maximum-scale=1, initial-scale=1, user-scalable=0" />
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta/css/bootstrap.min.css" integrity="sha384-/Y6pD6FV/Vv2HJnA6t+vslU6fwYXjCFtcEpHbNJ0lyAFsXTsjBbfaDjzALeQsN6M" crossorigin="anonymous" />        
      
        <?php include_once( dirname(__FILE__).'/../templates/seo.php'); ?>
    </head>

    <body>

        <!-- wrapper -->
        <div id="container">
          
            <div class="row justify-content-md-center mt-4">
                <div class="col-md-3 text-center">
                    <?php echo $this->website->name; ?>      
                </div>  
            </div>

        </div>
        <!-- /wrapper -->

    </body>
</html>