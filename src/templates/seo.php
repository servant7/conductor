<title><?php echo conductor\utility::issetor( $this->pageSEOTitle ) ?></title>

<meta name="description" content="<?php echo conductor\utility::issetor( $this->pageDescription ) ?>" />
<meta property="og:title" content="<?php echo conductor\utility::issetor( $this->pageSEOTitle ) ?>" />
<meta property="og:description" content="<?php echo conductor\utility::issetor( $this->pageDescription ) ?>" />
<meta property="og:url" content="//<?php echo $_SERVER['HTTP_HOST'].conductor\utility::issetor( $this->pageUrl, rtrim( $_SERVER['REQUEST_URI'], '/' ) ) ?>" /> 
<meta property="og:image" content="<?php echo conductor\utility::issetor( $this->pageImage ) ?>" />
<meta name="keywords" content="<?php echo conductor\utility::issetor( $this->pageKeywords ) ?>" />

<?php if( isset( $this->website->twitter ) ): ?>
<meta name="twitter:card" content="summary">
<meta name="twitter:site" content="<?php echo $this->website->twitter; ?>">
<meta name="twitter:creator" content="<?php echo $this->website->twitter; ?>">
<meta name="twitter:title" content="<?php echo conductor\utility::issetor( $this->pageSEOTitle ) ?>">
<meta name="twitter:description" content="<?php echo conductor\utility::issetor( $this->pageDescription ) ?>">

    <?php if( isset( $this->pageImageSquare ) ): ?>
    <meta name="twitter:image" content="//<?php echo $_SERVER['HTTP_HOST'].conductor\utility::issetor( $this->pageImageSquare ) ?>">
    <meta name="twitter:image:alt" content="<?php echo conductor\utility::issetor( $this->pageSEOTitle ) ?>">
    <?php endif;?>

<?php endif;?>

<meta name="robots" content="index, follow" />
<meta name="robots" content="all" />