<?php
header( 'HTTP/1.0 404 Not Found' );

/*Page Config*/
$this->pageTitle = 'Page not found - '.$this->domainInfo->domain;
$this->pageSEOTitle = $this->pageTitle;
$this->pageDescription = 'Sorry, the page you requested can not be found or is not currently available.';

?>
<!DOCTYPE html>
<html lang="en-us">

    <head>
        <meta charset="utf-8" />
        
        <!-- mobile settings -->
        <meta name="viewport" content="width=device-width, maximum-scale=1, initial-scale=1, user-scalable=0" />
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta/css/bootstrap.min.css" integrity="sha384-/Y6pD6FV/Vv2HJnA6t+vslU6fwYXjCFtcEpHbNJ0lyAFsXTsjBbfaDjzALeQsN6M" crossorigin="anonymous" />
        
        <?php include_once( 'seo.php'); ?>
    </head>

    <body class="">

        <!-- wrapper -->
        <div id="wrapper">

            <div class="container mt-4">

                <div class="row justify-content-center">

                    <div class="col-8 margin-top-40">

                        <h3 class="text-center"><span>Page Not Found</span>.</h3>
                        <p> 
                            Sorry, the page you requested can not be found or is not currently available. The page may have moved or you may have mis-typed the page URL. 
                            Please go to the <a href="/"><?php echo $this->website->name; ?> Home Page</a>&nbsp;to explore the many pages with our website.
                        </p>

                    </div>

                </div>

            </div>

        </div>
        <!-- /wrapper -->

    </body>
</html>
